import React, { Component } from 'react'
import ProtoTypes from 'prop-types'

class Page extends Component {
  render() {
    const { title } = this.props.data.datoCmsPage
    return <h1>{title}</h1>
  }
}

Page.protoTypes = {
  data: ProtoTypes.object.isRequired,
}
export default Page

export const pagesQuery = graphql`
  query pagesQuery($slug: String!) {
    datoCmsPage(slug: { eq: $slug }) {
      title
      slug
    }
  }
`
