import React from 'react'
import Link from 'gatsby-link'

const Page = ({ node }) => {
  return (
    <li>
      <Link to={node.slug}>{node.title}</Link>
    </li>
  )
}
const IndexPage = ({ data }) => (
  <ul>{data.allDatoCmsPage.edges.map(edge => <Page node={edge.node} />)}</ul>
)

export default IndexPage

export const pageQuery = graphql`
  query pageQuery {
    allDatoCmsPage(filter: { locale: { eq: "it" } }) {
      edges {
        node {
          title
          slug
        }
      }
    }
  }
`
